export function castTo(type: 'number' | 'boolean' | 'string') {
  return (target: any, propKey: string | symbol, descr?: any) => {
    let converter;
    switch (type) {
      case 'number':
        converter = (value): number => Number(value);
        break;
      case 'boolean':
        converter = (value): boolean => {
          if (value === 'false')
            return false;
          return Boolean(value);
        };
        break;
      case 'string':
        converter = (value): string => String(value);
        break;
      default:
        throw new Error(`Decorator @castTo. Unknown target type "${type}".`);
    }
    const {descriptor, receivedPrevDescriptor} = prepareAccessorDescriptor(target, propKey, descr, `@castTo('${type}')`)
    const resultDescriptor: PropertyDescriptor = {
      ...descriptor,
      get() {
        const value = descriptor.get.call(this);
        return converter(value);
      },
    };
    if (receivedPrevDescriptor)
      return resultDescriptor as any;
    resultDescriptor.configurable = true;
    Object.defineProperty(target, propKey, resultDescriptor);
  };
}


function prepareAccessorDescriptor(target, propKey: string | symbol, descr: PropertyDescriptor, decoratorName = '') {
  let descriptor: any = descr || Object.getOwnPropertyDescriptor(target, propKey) || getAccessorDescriptor();
  if (!descriptor.get && !descriptor.set)
    descriptor = getAccessorDescriptor(descriptor); // data-prop -> to accessor-prop
  else if (!descriptor.get && descriptor.set)
    throw new Error(`Decorator ${decoratorName}. Descriptor for prop "${String(propKey)}" has no getter, but has setter.`);
  /**
   * Здесь в результирующем descriptor'е:
   *   getter - обязательно присутствует;
   *   setter - может отсутствовать.
   */
  return {
    descriptor,
    receivedPrevDescriptor: !!descr, // для data-prop дескриптор не приходит в декоратор
  }
}

export function getAccessorDescriptor(base: PropertyDescriptor = {}): PropertyDescriptor {
  let value = base.value;
  return {
    get() {
      return value;
    },
    set(newValue) {
      value = newValue;
    },
    enumerable: base.enumerable ?? false,
    configurable: base.configurable ?? true,
  };
}
