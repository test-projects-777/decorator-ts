import {dataToAccessorProp} from './data-to-accessor-prop.decorator';
import {forCheckNext} from './for-check-next';
import {castTo} from './cast-to.decorator';

export class Some {

  @forCheckNext
  @castTo('number')
  @dataToAccessorProp
  name: string;

  constructor(name: string) {
    this.name = name;
  }

  @forCheckNext
  @castTo('string')
  get age(){
    return 21;
  }


}
