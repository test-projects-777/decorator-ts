import {Some} from './Some';

export function runToType() {
  const some = new Some('Alex');
  console.log(`prop "name"`, typeof some.name, some.name);
  console.log(`prop "age"`, typeof some.age, some.age);
  // console.log(`Object.getOwnPropertyDescriptor(some, 'name')`, Object.getOwnPropertyDescriptor(Some.prototype, 'name'));
}
