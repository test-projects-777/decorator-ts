import {printDecoratorInfo} from '../../../util';

export function forCheckNext(target: object, propKey: string | symbol, descriptor?: any) {
  printDecoratorInfo('forCheckNext',[
    [`propKey`, propKey],
    [`descriptor`, descriptor, descriptor?.get.toString()],
  ]);
}
