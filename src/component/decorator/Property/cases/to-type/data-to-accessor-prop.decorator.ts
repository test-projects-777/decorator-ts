import {getAccessorDescriptor} from './cast-to.decorator';

export function dataToAccessorProp(target: any, propKey: string | symbol, descriptor?: any) {
  if (descriptor && descriptor.get)
    return descriptor;
  console.log(`data-to-accessor`, )
  return getAccessorDescriptor(descriptor);
}
