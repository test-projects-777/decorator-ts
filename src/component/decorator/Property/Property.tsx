import {runToType} from './cases/to-type';

export function Property() {
  const handleRunToType = () => runToType();
  return (
    <div>
      <h2>Property класса</h2>
      <button onClick={handleRunToType}>run @toType</button>
    </div>
  );
}
