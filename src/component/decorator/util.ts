export function printDecoratorInfo(name: string, data: Array<any[]>) {
  console.log(``, )
  console.log(`@${name}`, )
  console.log(`---------------`, )
  data.forEach(x => {
    console.log(...x)
  });
  console.log(`===============`, )
  console.log(``, )
}
