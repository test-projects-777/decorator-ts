import {lazyPermanent} from './lazy-permanent.decorator';

export class Some {

  @lazyPermanent
  get my1Accessor() {
    console.log(`run Some.my1`, )
    return new My1();
  }


}

class My1 {

  constructor() {
    console.log(`creating M1`, )
  }

}
