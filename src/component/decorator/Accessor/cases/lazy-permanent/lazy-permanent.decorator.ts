
export function lazyPermanent(target, prop, descr?): PropertyDescriptor {
  return {
    get() {
      console.log(`lazyPermanent get()`, )
      const result = descr.get.call(this);
      Object.defineProperty(this, prop, {
        value: result
      })
      return result;
    },
    configurable: true
  }
}
