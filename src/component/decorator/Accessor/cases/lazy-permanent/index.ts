import {Some} from './Some';

export function runLazyPermanentAccessor(){
  const some = new Some();
  console.log(`invoke accessor`, some.my1Accessor)
  console.log(`invoke accessor`, some.my1Accessor)
  console.log(`invoke accessor`, some.my1Accessor)
  console.log(`invoke accessor`, some.my1Accessor)
  console.log(`invoke accessor`, some.my1Accessor)
  console.log(`invoke accessor`, some.my1Accessor)
}

export function runLazyPermanentDataProp(){
  const some = new Some();
  console.log(`invoke accessor`, some.my1Accessor)
  console.log(`invoke accessor`, some.my1Accessor)
  console.log(`invoke accessor`, some.my1Accessor)
  console.log(`invoke accessor`, some.my1Accessor)
  console.log(`invoke accessor`, some.my1Accessor)
  console.log(`invoke accessor`, some.my1Accessor)
}
