import {runLazyPermanentAccessor} from './cases/lazy-permanent';

export function Accessor() {
  const handleLazyPermanentAccessor = () => runLazyPermanentAccessor();
  return (
    <div>
      <h2>Accessor</h2>
      <button onClick={handleLazyPermanentAccessor}>run @lazyPermanent for accessor</button>
    </div>
  );
}
