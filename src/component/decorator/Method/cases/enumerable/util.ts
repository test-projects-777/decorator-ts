export function propsIn(obj) {
  const res: string[] = [];
  for (const prop in obj) {
    res.push(prop);
  }
  return res;
}
