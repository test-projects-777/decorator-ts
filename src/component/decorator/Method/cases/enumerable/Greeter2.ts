
export function Greeter2(this: any, message: string) {
  this.greeting = message;

  this.greet = function () {
    return 'Hello, ' + this.greeting;
  }

}
