import {printDecoratorInfo} from '../../../util';

export function enumerable(value: boolean): MethodDecorator {
  return function <T>(
    target: any,
    propKey: string | symbol,
    propDescr: TypedPropertyDescriptor<T>
  ) {
    printDecoratorInfo('enumerable', [
      [`target`, target],
      [`propKey`, typeof propKey === 'string' ? `"${propKey}"` : propKey],
      [`propDescr`, {...propDescr}],
    ]);
    propDescr.enumerable = value;
  };
}
