import {Greeter} from './Greeter';
import {propsIn} from './util';

export function runEnumerable() {
  const constructor = Greeter;
  // const constructor = Greeter2;
  const greeter = new constructor('world');
  console.log(`greet() =>`, greeter.greet());
  console.log(`greet() =>`, greeter.greet());
  console.log(`Object.keys`, Object.keys(greeter), Object.keys(constructor));
  console.log(`Object.getOwnPropertyNames`, Object.getOwnPropertyNames(greeter));
  console.log(`props in`, propsIn(greeter), propsIn(constructor));

  const proto = Object.getPrototypeOf(greeter);
  console.log(`Object.getOwnPropertyDescriptor(greeter, 'greet')`, Object.getOwnPropertyDescriptor(greeter, 'greet'))
  console.log(`Object.getOwnPropertyDescriptor(Prototype, 'greet')`, Object.getOwnPropertyDescriptor(proto, 'greet'))
  // console.log(`Object.getOwnPropertyDescriptors(Greeter)`, Object.getOwnPropertyDescriptors(Greeter));
  // console.log(`Object.getOwnPropertyDescriptors(Greeter2)`, Object.getOwnPropertyDescriptors(Greeter2));
}

