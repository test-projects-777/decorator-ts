import {enumerable} from './enumerable';

export class Greeter {
  constructor(public greeting: string) {
  }

  @enumerable(true)
  greet() {
    return 'Hello, ' + this.greeting;
  }
}
