import {runEnumerable} from './cases/enumerable';

export function Method() {
  const handleRunEnumerable = () => runEnumerable();
  return (
    <div>
      <h2>Method класса</h2>
      <button onClick={handleRunEnumerable}>run @enumerable</button>
    </div>
  );
}


