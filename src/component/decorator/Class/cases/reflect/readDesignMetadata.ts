import {isDiResponsibility} from './checkDiResponsibility';
import {printDecoratorInfo} from '../../../util';

export function readClassDesignMetadata(target: object) {
  readDesignMetadata(target);
}

function readDesignMetadata(target: object, propKey?: string | symbol, propDescr?: PropertyDescriptor) {
  const designType = Reflect.getOwnMetadata('design:type', target);
  const designParamtypes = Reflect.getOwnMetadata('design:paramtypes', target);
  const designReturntype = Reflect.getOwnMetadata('design:returntype', target);
  const checkParamtypes = designParamtypes?.map((x, i) => {
    // const {isOk, rejectedType} = checkDiResponsibility(x);
    // console.log(`  ${i}. isOk ${isOk}`, rejectedType || '      ', String(x))
    return `${isDiResponsibility(x) ? 'isOk' : '--'}`;
  })
  printDecoratorInfo('readDesignMetadata', [
    [`target.name`, target?.['name']],
    [`propKey`, propKey],
    [`propDescr`, propDescr],
    [`design:type`, designType],
    [`design:paramtypes`, designParamtypes, checkParamtypes],
    [`design:returntype`, designReturntype],
  ]);
}
