
export function isDiResponsibility(x: any): boolean {
  return String(x).startsWith('class');
}

export function checkDiResponsibility(x: any): IDiResponsibilityResp {
  const resp: IDiResponsibilityResp = {isOk: false};
  switch (x) {
    case Boolean:
      resp.rejectedType = 'boolean';
      return resp;
    case Number:
      resp.rejectedType = 'number';
      return resp;
    case BigInt:
      resp.rejectedType = 'bigint';
      return resp;
    case String:
      resp.rejectedType = 'string';
      return resp;
    case Symbol:
      resp.rejectedType = 'symbol';
      return resp;
    case Object:
      resp.rejectedType = 'object';
      return resp;
    default: // x {undefined, null, function, object}
      const type = typeof x;
      if (x == null) {
        resp.rejectedType = type;
        return resp;
      }
      // const proto = Object.getPrototypeOf(x); // x {function, object}
      const proto = x.prototype; // x {function, object}
      if (!proto || !proto['constructor']) {
        resp.rejectedType = 'not-constructable';
        return resp;
      }
      if (String(proto).includes('native code')) {
        resp.rejectedType = 'built-in';
        return resp;
      }
  }
  return {isOk: true};
}

interface IDiResponsibilityResp {
  isOk: boolean;
  rejectedType?: 'undefined' | 'boolean' | 'number' | 'bigint' | 'string' | 'symbol' | 'function' | 'object' | 'not-constructable' | 'built-in';
}
