import {readClassDesignMetadata} from './readDesignMetadata';
import {Check} from './Check';

@readClassDesignMetadata
export class SomeClass {

  constructor(public bool: boolean,
              public num: number,
              public bigint: bigint,
              public str: string,
              public symbol: symbol,
              public check: Check,
              public map: Map<string, object>,
              public some?) {
  }

}
