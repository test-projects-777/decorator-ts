import {SomeClass} from './SomeClass';
import {Check} from './Check';

export function runReadDesignMetadata() {
  console.log(`run readDesignMetadata`,)
  new SomeClass(false, 1, 1n, '1', Symbol(), new Check(), new Map());
}
