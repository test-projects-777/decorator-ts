import {runReadDesignMetadata} from './cases/reflect';

export function Class() {
  const handleRunReadDesignMetadata = () => runReadDesignMetadata();
  return (
    <div>
      <h2>Class</h2>
      <button onClick={handleRunReadDesignMetadata}>run @readDesignMetadata</button>
    </div>
  );
}
