import {Link} from '@do-while-for-each/browser-router-react-tools';
import style from './App.module.css'

export function App() {
  return (
    <div className={style.container}>
      <h2>Декоратор</h2>
      <Link href="/class">Class</Link><br/><br/>
      <Link href="/property">Property класса</Link><br/><br/>
      <Link href="/accessor">Accessor класса</Link><br/><br/>
      <Link href="/method">Method класса</Link><br/><br/>
    </div>
  );
}
