import {NotFoundPage} from '@do-while-for-each/browser-router-react-tools';
import {IEntry} from '@do-while-for-each/browser-router';
import {Accessor, Class, Method, Property} from '../component/decorator';
import {App} from '../component/App/App';

export const routes: IEntry[] = [
  {segment: '', component: <App/>},
  {segment: 'class', component: <Class/>},
  {segment: 'property', component: <Property/>},
  {segment: 'accessor', component: <Accessor/>},
  {segment: 'method', component: <Method/>},
  {segment: 'not-found', component: <NotFoundPage/>},
  {segment: '**', redirectTo: '/not-found'}
]
